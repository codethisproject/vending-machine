#include <iostream>
#include <string>
#include<bits/stdc++.h>
#include <map>
#include <typeinfo>
#include <iomanip>      // std::get_money
#include <cstdlib> //meh
#include <cctype>
#include <cstring>

using namespace std;

int inputCashNumber;
double inputCashAmount;
double oldMoney;
char drinkChoice;
char userResponse = 'y';
char additionalDrinkResponse;

double cashAmount [] = {1,5,10,20};
map<char,int> letterEquivalent;
double price;


void changeToUpperCase(char responseUpper){
    //Not needed
    cout << "changeToUpperCase --> Changed to Upper : " << putchar (toupper(responseUpper)) << endl;
}


void welcomeMessage(){
    cout << "=========================================" << endl;
    cout << "Welcome to the GATORS Vending Machine" << endl;
    cout << "=========================================" << endl;
    changeToUpperCase(userResponse);
}



void userDrinkSelection(char letterToConvert){

    letterEquivalent['A']=0;
    letterEquivalent['B']=1;
    letterEquivalent['C']=2;
    letterEquivalent['G']=3;
    double costOfDrink [] = {1.50, 2.00, 2.00, 2.25};
    price = costOfDrink[letterEquivalent[letterToConvert]];
    get_money(price);

}
void enterDollarAmount_Error(){
    while(cin.fail()){
        cin.clear();
        cin.ignore(256,'\n');
        cout << "enterDollarAmount_Error --> ERROR You did not even type a number:" << endl;
        cout << "enterDollarAmount_Error --> Old Money : " << oldMoney << endl;
        cout << "enterDollarAmount_Error --> Please insert your coins/bills" << endl;
        cout << "enterDollarAmount_Error --> (1)$1, (2)$5, (3)$10, (4)$20 :" << endl;
        cin >> inputCashNumber; // Equals 1,2,3,4
    }

}

void enterDollarAmount(){ //Works
    double oldMoney = inputCashAmount; //Holds current value
    cout << "enterDollarAmount --> Old Money : " << oldMoney << endl;
    cout << "enterDollarAmount --> Please insert your coins/bills" << endl;
    cout << "enterDollarAmount --> (1)$1, (2)$5, (3)$10, (4)$20 :" << endl;
    cin >> inputCashNumber; // Equals 1,2,3,4


    enterDollarAmount_Error();
    while(inputCashNumber > 4){
        cout << "enterDollarAmount --> Number is NOT within range" << endl;
        cout << "enterDollarAmount --> Old Money : " << oldMoney << endl;
        cout << "enterDollarAmount --> Please insert your coins/bills" << endl;
        cout << "enterDollarAmount --> (1)$1, (2)$5, (3)$10, (4)$20 :" << endl;
        cin >> inputCashNumber; // Equals 1,2,3,4
    }


    //Put Error Function here calling argument --> inputCashAmount
    cout << "enterDollarAmount --> inputCashAmount Before IF : $" << inputCashAmount << endl;
    cout << "enterDollarAmount --> oldMoney Before IF : $" << oldMoney << endl;


    if(oldMoney > 0){
        inputCashAmount = cashAmount[inputCashNumber -1] + oldMoney;
        cout << "enterDollarAmount --> INSIDE IF Your Choice : " << inputCashNumber << endl;
        cout << "enterDollarAmount --> INSIDE IF Money Amount : $" << inputCashAmount << endl;
    }
    else{
        inputCashAmount = cashAmount[inputCashNumber -1]; //Equals $1,$5,$10,$20
        cout << "enterDollarAmount --> ELSE Your Choice : " << inputCashNumber << endl;
        cout << "enterDollarAmount --> ELSE Money Amount : $" << inputCashAmount << endl;

    }
    cout << "enterDollarAmount --> Your Choice : " << inputCashNumber << endl;
    cout << "enterDollarAmount --> Money Amount : $" << inputCashAmount << endl;


}

void drinkError_NotAChoice(){

    while(isalpha(drinkChoice)){
        if(drinkChoice == 'A' || drinkChoice == 'a'){
            break;
        }
        else if(drinkChoice == 'B' || drinkChoice == 'b'){
            break;
        }
        else if(drinkChoice == 'C' || drinkChoice == 'c'){
            break;
        }
        else if(drinkChoice == 'G' || drinkChoice == 'g'){
            break;
        }
        cout << "drinkError_NotAChoice() --> ERROR the character you typed did not match! :" << endl;
        cout << "drinkError_NotAChoice() --> Please make a selection:" << endl;
        cout << "drinkError_NotAChoice() --> (A)quaVeena $1.50, (B)epsi $2.00, (C)ool Cola $2.00, (G)atorade $2.25" << endl;
        cin >> drinkChoice;
    }
}

void drinkSelectionQuestion(){
    cout << "drinkSelectionQuestion --> Please make a selection:" << endl;
    cout << "drinkSelectionQuestion --> (A)quaVeena $1.50, (B)epsi $2.00, (C)ool Cola $2.00, (G)atorade $2.25" << endl;
    cin >> drinkChoice;
    drinkError_NotAChoice();

    while(isalpha(drinkChoice) == false){
        cout << "drinkSelectionQuestion --> ERROR You did not even type a character:" << endl;
        cout << "drinkSelectionQuestion --> Please make a selection:" << endl;
        cout << "drinkSelectionQuestion --> (A)quaVeena $1.50, (B)epsi $2.00, (C)ool Cola $2.00, (G)atorade $2.25" << endl;
        cin >> drinkChoice;
        drinkError_NotAChoice();
    }

    cout << "drinkSelectionQuestion --> Drink Letter Chosen: " << drinkChoice << endl;
    cout << "drinkSelectionQuestion --> Money Amount : $" << inputCashAmount << endl;
    oldMoney = inputCashAmount;
    cout << "drinkSelectionQuestion --> Old Money : $" << oldMoney << endl;

}

void addMoreCoinsBillsQuestion(){
    cout << "addMoreCoinsBillsQuestion --> Add more coins/bills? (Y/N)" << endl;
    cin >> userResponse;
    changeToUpperCase(userResponse);
    while(userResponse){
        if(userResponse == 'n' || userResponse == 'N' ){
            cout << "addMoreCoinsBillsQuestion --> Ok buy something then" << endl;
            break;
        }
        else if(userResponse == 'y'|| userResponse == 'Y' ){
            cout << "additionalDrinkQuestion --> Sweet insert more money" << endl;
            break;
        }
        cout << "addMoreCoinsBillsQuestion --> Screw you type what I want! (Y/N)" << endl;
        cout << "addMoreCoinsBillsQuestion --> Add more coins/bills? (Y/N)" << endl;
        cin >> userResponse;
    }
}

void returnChange(){
    cout << "returnChange --> Take your change please : $" << inputCashAmount << endl;
    exit(10);
}

void insuffientFunds(double fundAmount){

        if(inputCashAmount == 0){
            returnChange();
        }
        else if(inputCashAmount < fundAmount){
            cout << "insuffientFunds --> You have insuffient Funds!" << endl;
            addMoreCoinsBillsQuestion();

        }
        else{
             double oldMoney = inputCashAmount; //Holds current value
             cout << "insuffientFunds --> Before : $" << oldMoney << endl;
             //inputCashAmount = oldMoney -= fundAmount; //Equals $1,$5,$10,$20
             inputCashAmount = oldMoney -= price; //Equals $1,$5,$10,$20
             cout << "insuffientFunds --> After : $" << oldMoney << endl;
             cout << "subtractDrinkPrice --> Price after Deducatble: $" << inputCashAmount << endl;
        }

}




void additionalDrinkQuestion(){
    additionalDrinkResponse = 'y';
    cout << "additionalDrinkQuestion --> Add more drinks (Y/N)" << endl;
    cin >> additionalDrinkResponse;
    while(additionalDrinkResponse){
        if(additionalDrinkResponse == 'n' || additionalDrinkResponse == 'N'){
            cout << "additionalDrinkQuestion --> Thank you & goodbye!" << endl;
            returnChange();
        }
        else if(additionalDrinkResponse == 'y' || additionalDrinkResponse == 'Y'){
            cout << "additionalDrinkQuestion --> Sweet lets buy more stuff" << endl;
            break;
        }
        cout << "You screwed up" << endl;
        cout << "additionalDrinkQuestion --> Add more drinks (Y/N)" << endl;
        cin >> additionalDrinkResponse;
    }
}



int main() {
    welcomeMessage();
    while(userResponse == 'y' || userResponse == 'Y'){
        enterDollarAmount();
        addMoreCoinsBillsQuestion();

        while(userResponse == 'n' || userResponse == 'N'){
            drinkSelectionQuestion();
            userDrinkSelection(drinkChoice);
            insuffientFunds(price);
            additionalDrinkQuestion();
        }
    }


    cout << "=================================" << endl;
    cout << "main --> inputcash = : $" << inputCashAmount << endl;
    cout << "main --> Drink Letter Chosen: " << drinkChoice << endl;
    cout << "main --> Price : $" << price << endl;
}
