#include <iostream>
#include <string>
#include<bits/stdc++.h>
#include <map>
#include <typeinfo>
#include <iomanip>      // std::get_money
#include <cstdlib> //meh
#include <cctype>
#include <cstring>

using namespace std;

int inputCashNumber;
double inputCashAmount;
double oldMoney;
char drinkChoice;
char userResponse = 'y';
char additionalDrinkResponse;

double cashAmount [] = {1,5,10,20};
map<char,int> letterEquivalent;
double price;




void welcomeMessage(){
    cout << "=========================================" << endl;
    cout << "Welcome to the GATORS Vending Machine" << endl;
    cout << "=========================================" << endl;
}



void userDrinkSelection(char letterToConvert){

    letterEquivalent['A']=0;
    letterEquivalent['B']=1;
    letterEquivalent['C']=2;
    letterEquivalent['G']=3;
    letterEquivalent['a']=0;
    letterEquivalent['b']=1;
    letterEquivalent['c']=2;
    letterEquivalent['g']=3;
    double costOfDrink [] = {1.50, 2.00, 2.00, 2.25};
    price = costOfDrink[letterEquivalent[letterToConvert]];
    get_money(price);

}
void enterDollarAmount_Error(){
    while(cin.fail()){
        cin.clear();
        cin.ignore(256,'\n');
        cout << "Invalid Selection: Please type a number 1-4" << endl;
        cout << "Please insert your coins/bills" << endl;
        cout << "(1)$1, (2)$5, (3)$10, (4)$20 :" << endl;
        cin >> inputCashNumber; // Equals 1,2,3,4
    }

}

void enterDollarAmount(){ //Works
    double oldMoney = inputCashAmount; //Holds current value
    cout << "Please insert your coins/bills" << endl;
    cout << "(1)$1, (2)$5, (3)$10, (4)$20 :" << endl;
    cin >> inputCashNumber; // Equals 1,2,3,4


    enterDollarAmount_Error();
    while(inputCashNumber > 4){
        cout << "Invalid Selection : Number is not within range." << endl;
        cout << "Please insert your coins/bills" << endl;
        cout << "(1)$1, (2)$5, (3)$10, (4)$20 :" << endl;
        cin >> inputCashNumber; // Equals 1,2,3,4
    }

    if(oldMoney > 0){
        inputCashAmount = cashAmount[inputCashNumber -1] + oldMoney;
    }
    else{
        inputCashAmount = cashAmount[inputCashNumber -1]; //Equals $1,$5,$10,$20
    }

}

void drinkError_NotAChoice(){

    while(isalpha(drinkChoice)){
        if(drinkChoice == 'A' || drinkChoice == 'a'){
            break;
        }
        else if(drinkChoice == 'B' || drinkChoice == 'b'){
            break;
        }
        else if(drinkChoice == 'C' || drinkChoice == 'c'){
            break;
        }
        else if(drinkChoice == 'G' || drinkChoice == 'g'){
            break;
        }
        cout << "Invalid Selection: Please use a letter (A,B,C,G) :" << endl;
        cout << "Please make a selection:" << endl;
        cout << "(A)quaVeena $1.50, (B)epsi $2.00, (C)ool Cola $2.00, (G)atorade $2.25" << endl;
        cin >> drinkChoice;
    }
}

void drinkSelectionQuestion(){
    cout << "Please make a selection:" << endl;
    cout << "(A)quaVeena $1.50, (B)epsi $2.00, (C)ool Cola $2.00, (G)atorade $2.25" << endl;
    cin >> drinkChoice;
    drinkError_NotAChoice();

    while(isalpha(drinkChoice) == false){
        cout << "Invalid Selection" << endl; //Not a Character
        cout << "Please make a selection:" << endl;
        cout << "(A)quaVeena $1.50, (B)epsi $2.00, (C)ool Cola $2.00, (G)atorade $2.25" << endl;
        cin >> drinkChoice;
        drinkError_NotAChoice();
    }
    oldMoney = inputCashAmount;
}

void addMoreCoinsBillsQuestion(){
    cout << "You've inserted : $" << cashAmount[inputCashNumber -1] << endl;
    cout << "Funds available : $" << inputCashAmount << endl;
    cout << "Add more coins/bills? (Y/N)" << endl;
    cin >> userResponse;
    while(userResponse){
        if(userResponse == 'n' || userResponse == 'N' ){
            break;
        }
        else if(userResponse == 'y'|| userResponse == 'Y' ){
            break;
        }
        cout << "Invalid Selection" << endl;
        cout << "Funds available : $" << inputCashAmount << endl;
        cout << "Add more coins/bills? (Y/N)" << endl;
        cin >> userResponse;
    }
}

void returnChange(){
    cout << "Please take your change : $" << inputCashAmount << endl;
    cout << "Thank you for using our vending machine!" << endl;
    exit(10);
}

void insuffientFunds(double fundAmount){

        if(inputCashAmount == 0){
            returnChange();
        }
        else if(inputCashAmount < fundAmount){
            cout << "Insufficient funds to make a purchase." << endl;
            addMoreCoinsBillsQuestion();
        }
        else{
             double oldMoney = inputCashAmount; //Holds current value
             inputCashAmount = oldMoney -= price; //Equals $1,$5,$10,$20
        }

}


void additionalDrinkQuestion(){
    additionalDrinkResponse = 'y';
    cout << "Funds available : $" << inputCashAmount << endl;
    cout << "Add more drinks (Y/N)" << endl;
    cin >> additionalDrinkResponse;
    while(additionalDrinkResponse){
        if(additionalDrinkResponse == 'n' || additionalDrinkResponse == 'N'){
            returnChange();
        }
        else if(additionalDrinkResponse == 'y' || additionalDrinkResponse == 'Y'){
            break;
        }
        cout << "Invalid Selection" << endl;
        cout << "Add more drinks (Y/N)" << endl;
        cin >> additionalDrinkResponse;
    }
}



int main() {
    welcomeMessage();
    while(userResponse == 'y' || userResponse == 'Y'){
        enterDollarAmount();
        addMoreCoinsBillsQuestion();

        while(userResponse == 'n' || userResponse == 'N'){
            drinkSelectionQuestion();
            userDrinkSelection(drinkChoice);
            insuffientFunds(price);
            additionalDrinkQuestion();
        }
    }
}
